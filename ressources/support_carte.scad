
$fn=180; 

material_thickness =  4.00; // Wall thickness
bolt_size = 3.5; // Size of connector bolts (M3) to link brackets together.
servo_bolt_size = 3.0; // Size of screws to connect brrackets to Servo Horns


difference() {
    union() {
        cube([65,65, material_thickness]);
        cube([3, 65, 20]);
        
        rotate([0,270,270])
         linear_extrude(4)
         polygon(points=[[0,0],[0,30],[20,0], [0,0]]);
        
        translate([0,61,0]) rotate([0,270,270])
         linear_extrude(4)
         polygon(points=[[0,0],[0,30],[20,0], [0,0]]);
    }
    // trou fixation plaque
    translate([0,13,10])
    rotate([0,90,0])
    cylinder(h = material_thickness, d = 3.5); 
    translate([0,53,10])
    rotate([0,90,0])
    cylinder(h = material_thickness, d = 3.5); 
        
    // trou fixation carte
    translate([10,8,0])
    cylinder(h = material_thickness, d = 2.5); 
    translate([10,34.7,0])
    cylinder(h = material_thickness, d = 2.5); 
    translate([10,57.5])
    cylinder(h = material_thickness, d = 2.5); 
}
