# mini-pong


Thisis a simple pong game using :
* PYBStick standard 26
* 1.3" 168x64 OLED screen
* linear potentiometers
* piezzo buzzer
* 10mm LEDs

![Circuit ](schemas.png)

 
Final assembly is using a lasercut plexigras holder and 3D printed gamepads. Game pads are a modified version of https://www.thingiverse.com/thing:4461152 to fit the potentiometers I used.

![PYBStick Mini Pong](photos/mini-pong-final.jpg)

You can find the components here :
* https://shop.mchobby.be/fr/micropython/1844-pybstick-standard-26-micropython-et-arduino-3232100018440-garatronic.html
* https://shop.mchobby.be/fr/minikits/67-assortiment-leds-10mm-mini-kit-3232100000674.html
* https://shop.mchobby.be/fr/raspberry-pi-a-b-plus/307-aff-graphique-oled-128x64-monochrome-3232100003071-adafruit.html
* https://shop.mchobby.be/fr/autres-capteurs/57-piezo-buzzer-3232100000575.html
* https://shop.mchobby.be/product.php?id_product=1922
