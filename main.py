# main.py -- put your code here
#
# The MIT License (MIT)
#
# Copyright (c) 2019 Garatronic
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import ssd1306, pyb
from machine import Pin, I2C
from pyb import Timer


# il faut minorer de 1 pixel pour prendre en compte le pixel 0
largeur_ecran = const (127)
hauteur_ecran = const (63)

largeur_raquette = const (4)
hauteur_raquette = const (10)
raquette_au_mur = const (10)

taille_balle = const (3)
vitesse_normale = const(20)
multiplicateur_vitesse = (1.3)

led_joueur_vert = Pin("S11", Pin.OUT)
led_joueur_jaune = Pin("S13", Pin.OUT)


adresse_afficheur = const (0x3D)
bus_i2c = I2C(sda=Pin("S3", mode=Pin.IN, pull=Pin.PULL_DOWN), scl=Pin("S5",  mode=Pin.IN, pull=Pin.PULL_DOWN), freq=400000) 
afficheur = ssd1306.SSD1306_I2C(128, 64, bus_i2c, 0x3D)

rst_pin_oled = Pin("S23", Pin.OUT)
rst_pin_oled.on()
pyb.delay(1)
rst_pin_oled.off()
pyb.delay(10)
rst_pin_oled.on()
  
afficheur.init_display()

# declaration de l'entree ADC pour mesure position potentiomètres
from pyb import ADC
potentiometre_raquette_vert = ADC(Pin('S8'))
potentiometre_raquette_jaune = ADC(Pin('S10'))
#ratio_entree_adc = 26.7
ratio_entree_adc = 77.2
#ratio_entree_adc = 64

# declaration du buzzer
sortie_buzzer = Pin('S24')
timer_buzzer = Timer(2, freq=2200)
pwm_buzzer=timer_buzzer.channel(1, Timer.PWM, pin=sortie_buzzer)

# Notes/tone with corresponding frequence
NOTES = { ' ' : 0,   # Silent
		  'c' : 261, # Do
		  'd' : 294, # Ré
		  'e' : 329, # Mi
		  'f' : 349, # Fa
		  'g' : 392, # Sol
		  'a' : 440, # La
		  'b' : 493, # Si
		  'C' : 523  # Do
		}
		
def jouer_frequence( freq ):
	if freq == 0:
		pwm_buzzer.pulse_width_percent( 0 )
	else:
		timer_buzzer.freq( freq )
		pwm_buzzer.pulse_width_percent( 30 )

def tone( freq, duration ):
	jouer_frequence( freq )
	time.sleep_ms( duration )
	jouer_frequence( 0 )
	time.sleep_ms( 20 )
	

# Variables de gestion des raquettes
precedente_position_Y_raquette_vert = 0
precedente_position_Y_raquette_jaune = 0
position_Y_raquette_vert = 0
position_Y_raquette_jaune = 0
vitesse_Y_raquette_vert = 0
vitesse_Y_raquette_jaune = 0
vitesse_Y_raquette_pour_effet = 2

position_X_balle = largeur_ecran / 2
position_Y_balle = hauteur_ecran / 2
vitesse_X_balle = 1
vitesse_Y_balle = 1

def clignotement_point(led, segment2=0):
    for i in range(3):
        led.on()
        pyb.delay(100)
        led.off()
        pyb.delay(100)

duree_bip=0

# boucle d'affichage
while (True):

  score_vert = 0
  score_jaune = 0

  position_X_balle = largeur_ecran / 2
  vitesse_X_balle = -1
  vitesse_Y_balle = 1
  vitesse_jeu = vitesse_normale


  while (score_vert < 10) and (score_jaune < 10):

    # Traitement du beep dans la boucle de $(vitesse_normale) ms
    if (duree_bip > 0):
      jouer_frequence(261)
      duree_bip -= 1
    else:
      jouer_frequence(0)

    # SECTION CALCUL ##############################################################
    debut_rafraichissement = pyb.millis()

    # calcul position de la raquette
    position_Y_raquette_vert = hauteur_ecran - hauteur_raquette - int (potentiometre_raquette_vert.read() / ratio_entree_adc) 
    position_Y_raquette_jaune = hauteur_ecran - hauteur_raquette - int (potentiometre_raquette_jaune.read() / ratio_entree_adc)

    # Determination du sens de progression de la raquette
    vitesse_Y_raquette_vert = position_Y_raquette_vert - precedente_position_Y_raquette_vert
    vitesse_Y_raquette_jaune = position_Y_raquette_jaune - precedente_position_Y_raquette_jaune


    # Sauvegarde position raquette avant nouvelle acquisition
    precedente_position_Y_raquette_vert = position_Y_raquette_vert
    precedente_position_Y_raquette_jaune = position_Y_raquette_jaune

    # Actualisation de la position de la balle en fonction de sa vitesse
    position_X_balle += vitesse_X_balle
    position_Y_balle += vitesse_Y_balle
    
    # Point marqué ? (pour le vert ou le jaune)
    if (position_X_balle >= largeur_ecran - taille_balle) or (position_X_balle <=0):
      # point marqué par le vert
      if (vitesse_X_balle > 0):
        score_vert +=1
        clignotement_point(led_joueur_vert)
        position_X_balle = int (largeur_ecran / 4)
      # point marqué par le jaune
      elif (vitesse_X_balle < 0):
        score_jaune +=1
        clignotement_point(led_joueur_jaune)
        position_X_balle = int ((largeur_ecran / 4) * 3)
      
      # on remet la balle au centre
      position_Y_balle = hauteur_ecran / 2
      vitesse_jeu = vitesse_normale 
      #beep_point()

    # Rebond en haut ou en bas : on inverse la vitesse Y de la balle
    elif (position_Y_balle >= hauteur_ecran - taille_balle) or (position_Y_balle <= 0):
        #beep_rebond()
        #_thread.start_new_thread(son_rebond_raquette(frequence=440, canal='A15', duree=100))
        vitesse_Y_balle *= -1
        duree_bip=2

    else:

      # Rebond sur raquette verte
      print(vitesse_Y_raquette_jaune)
      if (position_X_balle >= raquette_au_mur) and (position_X_balle <= raquette_au_mur + taille_balle) and (vitesse_X_balle < 0):
        if (position_Y_balle > position_Y_raquette_vert - taille_balle) and (position_Y_balle < position_Y_raquette_vert + hauteur_raquette):
          vitesse_X_balle *= -1
          if (vitesse_Y_raquette_vert * vitesse_Y_balle) > vitesse_Y_raquette_pour_effet:
            vitesse_jeu = vitesse_normale / multiplicateur_vitesse
          elif abs(vitesse_Y_raquette_vert * vitesse_Y_balle) >= vitesse_Y_raquette_pour_effet:
            vitesse_jeu = vitesse_normale * multiplicateur_vitesse
          else:
            vitesse_jeu = vitesse_normale
        duree_bip=2

      # Rebond sur raquette jaune
      elif (position_X_balle >= largeur_ecran - taille_balle- largeur_raquette - raquette_au_mur) and (position_X_balle <= largeur_ecran - taille_balle - raquette_au_mur) and (vitesse_X_balle > 0):
        if (position_Y_balle > position_Y_raquette_jaune - taille_balle) and (position_Y_balle < position_Y_raquette_jaune + hauteur_raquette):
          vitesse_X_balle *= -1
          if (vitesse_Y_raquette_jaune * vitesse_Y_balle) > vitesse_Y_raquette_pour_effet:
            vitesse_jeu = vitesse_normale * multiplicateur_vitesse
          elif abs(vitesse_Y_raquette_jaune * vitesse_Y_balle) >= vitesse_Y_raquette_pour_effet:
            vitesse_jeu = vitesse_normale / multiplicateur_vitesse
          else:
            vitesse_jeu = vitesse_normale
        duree_bip=2

    # SECTION AFFICHAGE ###############################################################
    afficheur.fill(0)

    # Affichage score
    afficheur.text("%d" % score_vert, int((largeur_ecran/2)-16), 0, 1)
    afficheur.text("%d" % score_jaune, int((largeur_ecran/2)+8), 0, 1)

    # Affichage balle
    afficheur.fill_rect(int(position_X_balle), int(position_Y_balle), taille_balle, taille_balle, 1)

    # Affichage raquette vert
    afficheur.fill_rect(raquette_au_mur, position_Y_raquette_vert, largeur_raquette, hauteur_raquette, 1)
    
    # Affichage raquette jaune
    afficheur.fill_rect(largeur_ecran - largeur_raquette - raquette_au_mur , position_Y_raquette_jaune, largeur_raquette, hauteur_raquette, 1)
  
    # Affichage ligne centrale (tirets 2 pixels, 2 vides)
    i=0
    while (i < hauteur_ecran):
      afficheur.line(int (largeur_ecran / 2), i, int (largeur_ecran / 2), i+1, 1)
      i += 4

    afficheur.show()
    while pyb.elapsed_millis(debut_rafraichissement) < vitesse_jeu:
      pass
      
  # affichage du vainceur
  if (score_vert == 10):
    afficheur.text("vert gagne !", 10, hauteur_ecran // 2)
    clignotement_point(led_joueur_vert)
  else:
    afficheur.text("jaune gagne !", 10, hauteur_ecran // 2)
    clignotement_point(led_joueur_jaune)
  afficheur.show()
  jouer_frequence(0)
  pyb.delay(5000)



